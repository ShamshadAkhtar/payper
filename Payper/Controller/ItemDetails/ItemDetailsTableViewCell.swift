//
//  ItemDetailsTableViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 17/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class ItemDetailsTableViewCell: UITableViewCell {

    @IBOutlet var outerview: UIView!
    @IBOutlet var countView: UIView!
    @IBOutlet var lblItemCount: UILabel!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblTotalPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
