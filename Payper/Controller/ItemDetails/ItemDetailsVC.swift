//
//  ItemDetailsVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 17/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class ItemDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MenuControllerDelegate {
    

    @IBOutlet var imgRestaurant: UIImageView!
    @IBOutlet var lblRestaurantName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var btnSideMenu: UIButton!
    var itemArray = ["Item Name"," Simply dummy text ","Item Name"," Simply dummy text","Item Name"," Simply dummy text","Item Name"," Simply dummy text","Item Name"," Simply dummy text "]
    
    override func viewDidLoad() {
        super.viewDidLoad()
   self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
      tableview.delegate =  self
      tableview.dataSource = self
        
    }
    
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func tabbar(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ItemDetailsTableViewCell
        cell.outerview.layer.cornerRadius =  cell.outerview.layer.frame.height / 2
        cell.countView.layer.cornerRadius = cell.countView.layer.frame.height / 2
        cell.lblItemName.text = itemArray[indexPath.row]
        //        cell.lblHeading.text = nameArray[indexPath.row] as? String
        //        cell.lblDescription.text = desArray[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
}
