//
//  SplitEqualyPaymentVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 12/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit


class SplitEqualyPaymentVC: UIViewController {

    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnback: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var upperTabBar: UIView!
    @IBOutlet var paybyitem: UIView!
    @IBOutlet var splitequaly: UIView!
    @IBOutlet var paycustom: UIView!
    @IBOutlet var sliderView: UIView!
    @IBOutlet var slider: UISlider!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblPercentage: UILabel!
    @IBOutlet var lblvalue: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var other_mainView: UIView!
    @IBOutlet var lblleft: UILabel!
    @IBOutlet var lblMiddle: UILabel!
    @IBOutlet var lblRight: UILabel!
    @IBOutlet var leftArrow: UIButton!
    @IBOutlet var rightArrow: UIButton!
    @IBOutlet var payfor_lblleft: UILabel!
    @IBOutlet var payfor_lblMiddle: UILabel!
    @IBOutlet var payfor_lblRight: UILabel!
    @IBOutlet var payfor_leftArrow: UIButton!
    @IBOutlet var payfor_rightArrow: UIButton!
    var l:Int!
    var m:Int!
    var r:Int!
    var l_payfor:Int!
    var m_payfor:Int!
    var r_payfor:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitequaly.backgroundColor = UIColor.init(red: 18/255, green: 149/255, blue: 174/255, alpha: 1)
        lblvalue.layer.cornerRadius = lblvalue.layer.frame.height / 2
        Design().SideRoundView(view: upperTabBar)
        Design().SideRoundView(view: mainView)
        Design().SideRoundView(view: other_mainView)
        Design().SideRoundButton(btn: btnNext)
        l = Int(lblleft.text!)
        m = Int(lblMiddle.text!)
        r = Int(lblRight.text!)
        l_payfor = Int(payfor_lblleft.text!)
        m_payfor = Int(payfor_lblMiddle.text!)
        r_payfor = Int(payfor_lblRight.text!)
    }
    
    
    @IBAction func btnNextClick(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PayWithPayperVC") as! PayWithPayperVC
        self.present(vc, animated: false)
    }
    
    
    @IBAction func sliderPressed(_ sender: UISlider) {
        DispatchQueue.main.async {
            let trackRect: CGRect  = self.slider.trackRect(forBounds: self.slider.bounds)
            let thumbRect: CGRect  = self.slider.thumbRect(forBounds: self.slider.bounds , trackRect: trackRect, value: self.slider.value)
            let x = thumbRect.origin.x + self.slider.frame.origin.x + 15
            let y = self.slider.frame.origin.y - 7
            let z =  self.slider.frame.origin.y + 40
            self.lblPercentage.center = CGPoint(x: x, y: z)
            self.lblvalue.center = CGPoint(x: x, y: y)
            let currentValue = Int(sender.value)
            self.lblPercentage.text = "\(currentValue)%"
            self.lblvalue.text = "\(currentValue*2)"
        }
        
        
    }
    
    
    @IBAction func menuClick(_ sender: Any) {
    }
    
    @IBAction func backClick(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func UpperTabBar(_ sender:UIButton)
        
    {
        
        switch sender.tag
        {
        case 0:
            print("PAY BY ITEM")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            self.present(vc, animated: false)
        case 1:
            print("SPLIT EQUALITY")
        case 2:
            print("PAY CUSTOM")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PayCustomVC") as! PayCustomVC
            self.present(vc, animated: false)
        default:
            break
        }
        
        
        
        
    }
    
    
    @IBAction func leftArrow(_ sender: Any) {
       if (l>0)
        {
            l = l - 1
            m = m - 1
            r = r - 1
        }
        else {}
        lblleft.text = String(l)
        lblMiddle.text = String(m)
        lblRight.text = String(r)
        
    }
    
    @IBAction func rightArrow(_ sender: Any) {
        
        
        
        
        l = l+1
        m = m+1
        r = r+1
        lblleft.text = String(l)
        lblMiddle.text = String(m)
        lblRight.text = String(r)
        
        
    }
    @IBAction func payfor_leftArrow(_ sender: Any) {
        if (l_payfor>0)
        {
            l_payfor = l_payfor - 1
            m_payfor = m_payfor - 1
            r_payfor = r_payfor - 1
        }
        else {}
        payfor_lblleft.text = String(l_payfor)
        payfor_lblMiddle.text = String(m_payfor)
        payfor_lblRight.text = String(r_payfor)
        
    }
    
    @IBAction func payfor_rightArrow(_ sender: Any) {
        
        
        
        
        l_payfor = l_payfor+1
        m_payfor = m_payfor+1
        r_payfor = r_payfor+1
        payfor_lblleft.text = String(l_payfor)
        payfor_lblMiddle.text = String(m_payfor)
        payfor_lblRight.text = String(r_payfor)
        
        
    }
    

}
