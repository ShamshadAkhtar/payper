//
//  ScanVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 17/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class ScanVC: UIViewController,MenuControllerDelegate,UITextFieldDelegate {

    @IBOutlet var scanView: UIView!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var txtInput: UITextField!
    @IBOutlet var btnJoin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Design().SideRoundButton(btn: btnJoin)
        Design().addPaddingAndBorder2(to: txtInput)
        txtInput.layer.cornerRadius = txtInput.frame.height / 2
        txtInput.clipsToBounds = true
        txtInput.attributedPlaceholder = NSAttributedString(string: "Input Manually",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtInput.delegate = self
        scanView.layer.cornerRadius = scanView.layer.frame.width / 2
        scanView.clipsToBounds = true
       
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
    }
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnScanClick(_ sender: Any) {
    }
    @IBAction func tabbar(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    @IBAction func btnJoinClick(_ sender: Any) {
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
