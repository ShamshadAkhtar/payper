//
//  AdditionalRegisterInfoVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 18/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class AdditionalRegisterInfoVC: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{

    
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var dateView: UIView!
    @IBOutlet var txtDate: UITextField!
    @IBOutlet var genderView: UIView!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var sliderView: UIView!
    @IBOutlet var cardView: UIView!
    @IBOutlet var txtCard: UITextField!
    @IBOutlet var slider: UISlider!
    @IBOutlet var lblTip: UILabel!
    @IBOutlet var imag_date: UIImageView!
    @IBOutlet var imag_gender: UIImageView!
    @IBOutlet var imag_card: UIImageView!
    var genderarray = [String]()
    var gender_pick :UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imag_date.image = #imageLiteral(resourceName: "calender")
        imag_gender.image = #imageLiteral(resourceName: "my_account")
        imag_card.image = #imageLiteral(resourceName: "card_info")
        genderarray = ["Male","Female","Others"]
        showDatePicker()
        gender_picker(txtGender)
        dateView.underlined2()
        genderView.underlined2()
        cardView.underlined2()
        Design().SideRoundButton(btn: btnSave)
        Design().textFieldFeture(txt: txtCard)
        Design().textFieldFeture(txt: txtDate)
        Design().textFieldFeture(txt: txtGender)
        txtDate.attributedPlaceholder = NSAttributedString(string: "Date",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        txtGender.attributedPlaceholder = NSAttributedString(string: "Gender",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtCard.attributedPlaceholder = NSAttributedString(string: "Card Info",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtGender.delegate = self
        txtDate.delegate = self
        txtCard.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSaveClick(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EnterOTPHereVC") as! EnterOTPHereVC
        self.present(vc, animated: false)
    }
    
    @IBAction func sliderPressed(_ sender: UISlider) {
        DispatchQueue.main.async {
            let trackRect: CGRect  = self.slider.trackRect(forBounds: self.slider.bounds)
            let thumbRect: CGRect  = self.slider.thumbRect(forBounds: self.slider.bounds , trackRect: trackRect, value: self.slider.value)
            let x = thumbRect.origin.x + self.slider.frame.origin.x + 15 
            //let y = self.slider.frame.origin.y - 10
            let z =  self.slider.frame.origin.y + 40
            self.lblTip.center = CGPoint(x: x, y: z)
          //  self.lblvalue.center = CGPoint(x: x, y: y)
            let currentValue = Int(sender.value)
            self.lblTip.text = "\(currentValue)%"
          //  self.lblvalue.text = "\(currentValue*2)"
        }
   }
    
    let datePicker = UIDatePicker()
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(AdditionalRegisterInfoVC.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(AdditionalRegisterInfoVC.cancelDatePicker))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        // add toolbar to textField
        txtDate.inputAccessoryView = toolbar
        // add datepicker to textField
        txtDate.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        txtDate.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderarray.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let status = "\(genderarray[row])"
         txtGender.text = status
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let status = "\(genderarray[row])"
        return status
    }
    func   get_gender()
    {
    self.gender_pick.dataSource = self
    self.gender_pick.delegate = self
    self.gender_pick.reloadAllComponents()
    }
    
    // picker for gender
    
    
    func gender_picker(_ textField : UITextField){
        self.gender_pick = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.gender_pick.backgroundColor = UIColor.white
        textField.inputView = self.gender_pick
        gender_pick.dataSource = self
        gender_pick.delegate = self
        get_gender()
        gender_pick.tag=0
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(AdditionalRegisterInfoVC.doneClick2))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(AdditionalRegisterInfoVC.cancelClick2))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneClick2() {
        txtGender.resignFirstResponder()
    }
    
    @objc func cancelClick2() {
        txtGender.resignFirstResponder()
    }
    //end picker for gender
    
    
}
