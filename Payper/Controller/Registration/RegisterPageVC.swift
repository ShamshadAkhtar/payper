
//
//  RegisterPageVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 17/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class RegisterPageVC: UIViewController,UITextFieldDelegate {
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var firstNameView:UIView!
    @IBOutlet var lastNameView:UIView!
    @IBOutlet var emailView:UIView!
    @IBOutlet var passwordView:UIView!
    @IBOutlet var imag_firstname:UIImageView!
    @IBOutlet var imag_lastname:UIImageView!
    @IBOutlet var imag_email:UIImageView!
    @IBOutlet var imag_password:UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imag_firstname.image = #imageLiteral(resourceName: "my_account")
        imag_lastname.image = #imageLiteral(resourceName: "my_account")
        imag_email.image = #imageLiteral(resourceName: "message")
        imag_password.image = #imageLiteral(resourceName: "password")
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtFirstName.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtLastName.attributedPlaceholder = NSAttributedString(string: "Last Name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtPassword.isSecureTextEntry = true
        firstNameView.underlined()
        lastNameView.underlined()
        emailView.underlined()
        passwordView.underlined()
        Design().SideRoundButton(btn: btnRegister)
        Design().textFieldFeture(txt: txtFirstName)
        Design().textFieldFeture(txt: txtLastName)
        Design().textFieldFeture(txt: txtEmail)
        Design().textFieldFeture(txt: txtPassword)
        
    }
    @IBAction func btnRegisterClick(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AdditionalRegisterInfoVC") as! AdditionalRegisterInfoVC
        self.present(vc, animated: false)
    }

    @IBAction func btnCancelClick(_ sender: Any) {
        txtFirstName.text = ""
        txtLastName.text = ""
        txtPassword.text = ""
        txtEmail.text = ""
    }
  
    @IBAction func btnLoginClick(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginPageVC") as! LoginPageVC
        self.present(vc, animated: false)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
