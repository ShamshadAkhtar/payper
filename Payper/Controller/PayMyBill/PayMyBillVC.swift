//
//  PayMyBillVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 07/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit
class PayMyBillVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MenuControllerDelegate {
 
    

    @IBOutlet var tableview: UITableView!
    @IBOutlet var viewUpperTabbar: UIView!
    @IBOutlet var btnPay: UIButton!
    @IBOutlet var lblTotalPrice: UILabel!
    @IBOutlet var infoView: UIView!
    @IBOutlet var menuView: UIView!
    @IBOutlet var connectView: UIView!
    @IBOutlet var shareView: UIView!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var scan: UIImageView!
    var itemArray : NSArray!
    let amount = "AED 500"
    override func viewDidLoad() {
        super.viewDidLoad()
         scan.image = #imageLiteral(resourceName: "scan(color)")
        lblTotalPrice.text = "Total Amount : " + amount
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        menuView.backgroundColor = UIColor.init(red: 18/255, green: 149/255, blue: 174/255, alpha: 1)
        Design().SideRoundView(view: viewUpperTabbar)
        Design().SideRoundButton(btn: btnPay)
        itemArray = ["Item Name"," Simply dummy text ","Item Name"," Simply dummy text","Item Name"," Simply dummy text","Item Name"," Simply dummy text","Item Name"," Simply dummy text "]
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PayMyBillTableViewCell
        cell.outerview.layer.cornerRadius =  cell.outerview.layer.frame.height / 2
        cell.countView.layer.cornerRadius = cell.countView.layer.frame.height / 2
        cell.lblItemName.text = itemArray[indexPath.row] as? String
//        cell.lblHeading.text = nameArray[indexPath.row] as? String
//        cell.lblDescription.text = desArray[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }

    @IBAction func btnPayClick(_ sender: Any) {
        
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        self.present(vc, animated: false)
        
    }
    
    
    @IBAction func btnMenuClick(_ sender: Any) {
    }
    
    
    
    
    
    
    
    
    
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    
    @IBAction func UpperTabBar(_ sender:UIButton)
    {
        switch sender.tag
        {
        case 0:
            print("info")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
        case 1:
            print("menu")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PayMyBillVC") as! PayMyBillVC
            self.present(vc, animated: false)
        case 2:
            print("connect")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "UserSharingTabVC") as! UserSharingTabVC
            self.present(vc, animated: false)
        case 3:
            print("share")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "BarCodeShareVC") as! BarCodeShareVC
            self.present(vc, animated: false)
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
}
