//
//  StartPageVC.swift
//  Happen
//
//  Created by CodeBuzzers on 22/11/18.
//  Copyright © 2018 Shamshad. All rights reserved.
//

import UIKit

class StartPageVC: UIViewController,UIScrollViewDelegate{
    
    
    @IBOutlet var scrlView: UIScrollView!
    
    @IBOutlet var bgImage: UIImageView!
    @IBOutlet var pageControl: UIPageControl!
    
    var slides : [Slide] = []
    var counter = 0
    //var img = ["ot","jt","pt"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrlView.delegate = self
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    override func viewWillAppear(_ animated: Bool) {
        scrlView.delegate = self
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    }
    
    
    
    @objc func createSlides() -> [Slide] {
        
        let slide1:Slide   = Bundle.main.loadNibNamed("StartPageVC", owner: self, options: nil)?.first as! Slide
        slide1.imgView.image    = UIImage(named: "ot")
        slide1.lblTitle.text = "Open Tab"
        slide1.lblSecondTitle.text = "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries"
       // slide1.lblSecondTitle.font = UIFont(name: "Montserrat-SemiBold", size: 37)
      //  slide1.lblThirdTitle.text = ""
      //slide1.lblDescription1.text = "Register, Collect and"
      //  slide1.lblDescription.text = "Reward Yourself!"
        
        let slide2:Slide       = Bundle.main.loadNibNamed("StartPageVC", owner: self, options: nil)?.first as! Slide
        slide2.imgView.image    = UIImage(named: "jt")
        
        slide2.lblTitle.text = "Join Tab"
        slide2.lblSecondTitle.text = "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries"
       // slide2.lblSecondTitle.font = UIFont(name: "Montserrat-SemiBold", size: 37)
       // slide2.lblThirdTitle.text = "Together"
       // slide2.lblDescription1.text = ""
       // slide2.lblDescription.text = "Register & Start SAVINGS!"
        
        let slide3:Slide       = Bundle.main.loadNibNamed("StartPageVC", owner: self, options: nil)?.first as! Slide
        slide3.imgView.image    = UIImage(named: "pt")
        slide3.lblTitle.text = "Pay Tab"
        slide3.lblSecondTitle.text = "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries"
       // slide3.lblDescription1.text = "Register, Collect and"
        //slide3.lblThirdTitle.text = "ALL the time"
       // slide3.lblDescription.text = "Reward Yourself!"
        
        
        return [slide1,slide2,slide3]
    }
    
    func setupSlideScrollView(slides : [Slide]) {
        scrlView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrlView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrlView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrlView.addSubview(slides[i])
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth : CGFloat = scrollView.frame.width
        let currentPage : CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        self.pageControl.currentPage = Int(currentPage)
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        if Int(currentPage) == 0 {
            bgImage.backgroundColor = UIColor(red: 47/255, green: 50/255, blue: 59/255, alpha: 1)
        } else if Int(currentPage) == 1 {
             bgImage.backgroundColor = UIColor(red: 47/255, green: 50/255, blue: 59/255, alpha: 1)
           // bgImage.image = UIImage(named: "XMLID_18_")
        } else if Int(currentPage) == 2 {
             bgImage.backgroundColor = UIColor(red: 47/255, green: 50/255, blue: 59/255, alpha: 1)
            //bgImage.image = UIImage(named: "icecream")
        } else {
             bgImage.backgroundColor = UIColor(red: 47/255, green: 50/255, blue: 59/255, alpha: 1)
            //bgImage.image = UIImage(named: "icecream")
        }
    }
    @objc func moveToNextPage (){
        
        let pageWidth:CGFloat = self.view.frame.width
        let contentOffset:CGFloat = self.scrlView.contentOffset.x
        let slideToX = contentOffset + pageWidth
        self.scrlView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrlView.frame.height), animated: true)
    }
    
    @IBAction func btnSkip(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "LoginPageVC") as! LoginPageVC
        self.present(vc, animated: false)
    
    }
    
}

