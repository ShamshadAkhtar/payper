//
//  MyTabTableViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 07/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class MyTabTableViewCell: UITableViewCell {

    @IBOutlet var outerview: UIView!
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
