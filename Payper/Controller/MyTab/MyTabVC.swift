//
//  MyTabVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 07/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit
class MyTabVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MenuControllerDelegate {
   
    
    
    @IBOutlet var viewUpperTabBar: UIView!
    @IBOutlet var btnInfo: UIButton!
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnConnect: UIButton!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var infoView: UIView!
    @IBOutlet var menuView: UIView!
    @IBOutlet var connectView: UIView!
    @IBOutlet var shareView: UIView!
    @IBOutlet var btnPay: UIButton!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var home: UIImageView!
    @IBOutlet var scan: UIImageView!
    @IBOutlet var sideMenu: UIImageView!
    var imgArray : [UIImage] = []
    var nameArray : NSArray!
    var desArray: NSArray!
    let amount = "AED 500"
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  home.image = #imageLiteral(resourceName: "home(white)")
        scan.image = #imageLiteral(resourceName: "scan(color)")
      //  sideMenu.image = #imageLiteral(resourceName: "sidemenu(white)")
        lblTotalAmount.text = "Total Amount : " + amount
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        infoView.backgroundColor = UIColor.init(red: 18/255, green: 149/255, blue: 174/255, alpha: 1)
        Design().SideRoundView(view: viewUpperTabBar)
        Design().SideRoundButton(btn: btnPay)
        imgArray = [#imageLiteral(resourceName: "location"),#imageLiteral(resourceName: "calender_page"),#imageLiteral(resourceName: "Icon_FastSetting2-512"),#imageLiteral(resourceName: "90-01-512"),#imageLiteral(resourceName: "BILL")]
        nameArray = ["Location","Date Opened","Time Opened","Overall Bill","Amount Paid"]
        desArray = ["Establishment Name","DD Month YYYY","HH:MM AM","AED XXX","AED XXX"]
        
    }
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MyTabTableViewCell
        cell.outerview.layer.cornerRadius =   cell.outerview.layer.frame.height / 2
        cell.imgLogo.image = imgArray[indexPath.row]
        cell.lblHeading.text = nameArray[indexPath.row] as? String
        cell.lblDescription.text = desArray[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
        
    }
    @IBAction func payClick(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        self.present(vc, animated: false)
    }
    
    
    @IBAction func upperMenu(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        self.present(vc, animated: false)
    }
    
    
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    
    @IBAction func UpperTabBar(_ sender:UIButton){
    
    switch sender.tag
    {
    case 0:
        print("info")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
        self.present(vc, animated: false)
    case 1:
        print("menu")
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PayMyBillVC") as! PayMyBillVC
        self.present(vc, animated: false)
    case 2:
        print("connect")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "UserSharingTabVC") as! UserSharingTabVC
        self.present(vc, animated: false)
    case 3:
        print("share")
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "BarCodeShareVC") as! BarCodeShareVC
        self.present(vc, animated: false)
    default:
        break
        }
    
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    
}
