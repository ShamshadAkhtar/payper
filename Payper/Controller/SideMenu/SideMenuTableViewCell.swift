//
//  SideMenuTableViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 09/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    @IBOutlet var img_menu: UIImageView!
    @IBOutlet var lblMenuName: UILabel!
    @IBOutlet var viewSeparetor: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
