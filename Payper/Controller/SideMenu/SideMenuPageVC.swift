//
//  SideMenuPageVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 09/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

protocol MenuControllerDelegate {
    
    func ShowHideMenuController(_ isShown:Bool)
   
}

class SideMenuPageVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var delegate: MenuControllerDelegate?
    fileprivate var targetController:UIViewController!
    
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var txtProfileName: UILabel!
    @IBOutlet var menu_Tableview: UITableView!

    var menu = [String]()
    var imgArray : [UIImage]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        imgProfile.image = #imageLiteral(resourceName: "profileimg")
        menu = ["Near Me","My Account","My Wallet","My Tabs(History)","Contact Us","Logout"]
        imgArray = [#imageLiteral(resourceName: "near_me"),#imageLiteral(resourceName: "my_account"),#imageLiteral(resourceName: "wallet"),#imageLiteral(resourceName: "my_tabs(history)"),#imageLiteral(resourceName: "contact_us"),#imageLiteral(resourceName: "logout")]
        menu_Tableview.delegate = self
        menu_Tableview.dataSource = self
      
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menu_Tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuTableViewCell
        cell.selectionStyle = .none
        cell.lblMenuName.text = menu[indexPath.row]
        cell.img_menu.image = imgArray[indexPath.row]
        if (indexPath.row == menu.count)
        {
            cell.viewSeparetor.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("near me")
            HideMenuController()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "NearMeVC") as! NearMeVC
            self.present(vc, animated: false)
           
        case 1:
            print("My Account")
            HideMenuController()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            self.present(vc, animated: false)
            
            
        case 2:
            print("My Wallet")
            HideMenuController()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
            self.present(vc, animated: false)
            
            
        case 3:
            print("My Tabs")
             HideMenuController()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SearchMyTabsVC") as! SearchMyTabsVC
            self.present(vc, animated: false)
           
        case 4:
            print("Contact Us")
             HideMenuController()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.present(vc, animated: false)
         
        case 5:
            print("Logout")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "LoginPageVC") as! LoginPageVC
            self.present(vc, animated: false)
        default:
            break
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        HideMenuController()
    }
    func ShowMenuInController(_ controller:UIViewController) {
        delegate?.ShowHideMenuController(true)
        self.targetController = controller
        self.view.backgroundColor = UIColor.clear      
        self.view.frame = CGRect(x: -self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        controller.addChild(self)
        controller.view.addSubview(self.view)
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
    
    func HideMenuController() {
        delegate?.ShowHideMenuController(false)
        self.view.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.6, delay: 0.0, options: .curveEaseIn, animations: {
            
            self.view.frame = CGRect(x: -self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.view.layoutIfNeeded()
            
        }, completion:{ finished in
            
            self.view.removeFromSuperview()
            self.removeFromParent()
            
            
        })
    }
}
