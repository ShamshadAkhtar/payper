//
//  PaymentVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 10/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
       // var count = [Int]()
   
        @IBOutlet var btnMenu: UIButton!
        @IBOutlet var btnback: UIButton!
        @IBOutlet var btnPay: UIButton!
        @IBOutlet var upperTabBar: UIView!
        @IBOutlet var paybyitem: UIView!
        @IBOutlet var splitequaly: UIView!
        @IBOutlet var paycustom: UIView!
        @IBOutlet var tableView: UITableView!
        @IBOutlet var sliderView: UIView!
        @IBOutlet var slider: UISlider!
        @IBOutlet var lblTotalAmount: UILabel!
        @IBOutlet var lblPercentage: UILabel!
        @IBOutlet var lblvalue: UILabel!
        var amount = "AED 500"
        var txt = "Your Subtotal"
        var item = [" Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."," Simply dummy text of the printing."]
        var price = ["@ AED 200 / each","@ AED 200 / each","@ AED 200 / each","@ AED 200 / each","@ AED 200 / each","@ AED 200 / each","@ AED 200 / each","@ AED 200 / each","@ AED 200 / each","@ AED 200 / each"]
        var Count = [0,2,1,0,3,2,0,4,7,2]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        paybyitem.backgroundColor = UIColor.init(red: 18/255, green: 149/255, blue: 174/255, alpha: 1)
        lblvalue.layer.cornerRadius = lblvalue.layer.frame.height / 2
        Design().SideRoundView(view: upperTabBar)
        Design().SideRoundButton(btn: btnPay)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return item.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let  cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PaymentTableViewCell
        cell.outerView.layer.cornerRadius =   cell.outerView.layer.frame.height / 2
       // cell.countView.layer.cornerRadius =   cell.countView.layer.frame.height / 2
        cell.valueChangeview.layer.cornerRadius = cell.valueChangeview.layer.frame.height / 2
        cell.lblItemName.text = item[indexPath.row]
        cell.lblEachItemPrice.text = price[indexPath.row]
        cell.lblCount.text = "\(Count[indexPath.row])"
        cell.btnincrement.tag = indexPath.row
        cell.btndecrement.tag = indexPath.row
        cell.btndecrement.addTarget(self, action: #selector(decrement(_:)), for: .touchUpInside)
        cell.btnincrement.addTarget(self, action: #selector(increment(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
    
    @IBAction func btnNextClick(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PayWithPayperVC") as! PayWithPayperVC
        self.present(vc, animated: false)
        
    }
    
    
    @IBAction func sliderPressed(_ sender: UISlider) {
        DispatchQueue.main.async {
            let trackRect: CGRect  = self.slider.trackRect(forBounds: self.slider.bounds)
            let thumbRect: CGRect  = self.slider.thumbRect(forBounds: self.slider.bounds , trackRect: trackRect, value: self.slider.value)
            let x = thumbRect.origin.x + self.slider.frame.origin.x + 16
            let y = self.slider.frame.origin.y - 10
            let z =  self.slider.frame.origin.y + 40
            self.lblPercentage.center = CGPoint(x: x, y: z)
            self.lblvalue.center = CGPoint(x: x, y: y)
            let currentValue = Int(sender.value)
            self.lblPercentage.text = "\(currentValue)%"
            self.lblvalue.text = "\(currentValue*2)"
           //  yourLabel.center = CGPoint(x: thumbRect.origin.x + self.slider.frame.origin.x + 30, y: self.slider.frame.origin.y - 60)
        }
        
        
    }
    
    
    @IBAction func menuClick(_ sender: Any) {
        
        
        
    }
    
    @IBAction func backClick(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func UpperTabBar(_ sender:UIButton)
        
    {
        
        switch sender.tag
        {
        case 0:
            print("PAY BY ITEM")
        case 1:
            print("SPLIT EQUALITY")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SplitEqualyPaymentVC") as! SplitEqualyPaymentVC
            self.present(vc, animated: false)
        case 2:
            print("PAY CUSTOM")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PayCustomVC") as! PayCustomVC
            self.present(vc, animated: false)
        default:
            break
        }
        
        
        
        
    }
    
    @objc func increment(_ sender: UIButton)
    {
        let i = sender.tag
        let myIndexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tableView.cellForRow(at: myIndexPath) as! PaymentTableViewCell
         Count[i] = Count[i] + 1
         cell.lblCount.text = "\(Count[i])"
    }
    
    @objc func decrement(_ sender: UIButton)
    {
        let i = sender.tag
        let myIndexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tableView.cellForRow(at: myIndexPath) as! PaymentTableViewCell
        if(Count[i] == 0){
            Count[i] = 0
        }else{
         Count[i] =  Count[i] - 1
        }
        cell.lblCount.text = "\(Count[i])"

    }
}
