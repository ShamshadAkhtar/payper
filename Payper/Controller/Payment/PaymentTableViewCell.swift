//
//  PaymentTableViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 11/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {

    
    
    @IBOutlet var outerView: UIView!
    @IBOutlet var lblItemName: UILabel!
    @IBOutlet var countView: UIView!
    @IBOutlet var lblEachItemPrice: UILabel!
    @IBOutlet var lblNoOfItem: UILabel!
    @IBOutlet var valueChangeview: UIView!
    @IBOutlet var lblCount: UILabel!
    @IBOutlet var btnincrement:UIButton!
    @IBOutlet var btndecrement:UIButton!
    
    var count :Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

//    @IBAction func addPressed(_ sender: UIButton) {
//        self.count += 1
//        self.lblCount.text = "\(self.count)"
//    }
}
