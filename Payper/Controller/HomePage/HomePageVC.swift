//
//  ViewController.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 06/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit
class HomePageVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,MenuControllerDelegate,UITextFieldDelegate
{
    // MARK:- @IBOutlets
    @IBOutlet var searchBarView: UIView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var home: UIImageView!
    @IBOutlet var scan: UIImageView!
    @IBOutlet var sideMenu: UIImageView!
    @IBOutlet var homeCollectionView: UICollectionView!
    
    let numberOfColumns: CGFloat = 2
    
    // MARK:- view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        home.image = #imageLiteral(resourceName: "home")
        scan.image = #imageLiteral(resourceName: "scan(white)")
        sideMenu.image = #imageLiteral(resourceName: "sidemenu(white)")
        Design().SideRoundView(view: searchBarView)
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        txtSearch.delegate = self
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search by Establishment",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    override func viewDidAppear(_ animated: Bool) {
      //  if (btnSideMenu.isUserInteractionEnabled == )
        if let flowLayout = homeCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout {

            let horizontalSpacing = flowLayout.scrollDirection == .vertical ? flowLayout.minimumInteritemSpacing : flowLayout.minimumLineSpacing

            let cellWidth = (homeCollectionView.frame.width - max(0, numberOfColumns - 1)*horizontalSpacing)/numberOfColumns

            flowLayout.itemSize = CGSize(width: cellWidth, height: cellWidth)

        }
    }
    // MARK:- collection view methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = homeCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HomeCollectionViewCell
        cell.layer.cornerRadius = 10
        cell.lblevent.layer.cornerRadius = cell.lblevent.frame.height / 2
        cell.clipsToBounds = true
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HotelDetailsVC") as! HotelDetailsVC
        self.present(vc, animated: false)
    }
    // MARK:- button actions
    @IBAction func btnSearchClick(_ sender: Any) {
    }
    
    // MARK:- Tabbar
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
         
        case 1:
            print("scan")
         
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    // MARK:- side menu delegates
    func ShowHideMenuController(_ isShown: Bool) {
        
        btnSideMenu.isUserInteractionEnabled = !isShown
     
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    
    // MARK:- text fiels delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

