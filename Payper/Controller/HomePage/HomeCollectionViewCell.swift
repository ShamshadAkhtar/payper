//
//  HomeCollectionViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 06/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
        @IBOutlet var imgevent: UIImageView!
        @IBOutlet var lblevent: UILabel!
        @IBOutlet var lblEventName: UILabel!
        @IBOutlet var lblEventDescription: UILabel!
        @IBOutlet var imgLogo: UIImageView!
        @IBOutlet var lblName: UILabel!
    
    
}
