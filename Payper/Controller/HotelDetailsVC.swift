//
//  HotelDetailsVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 14/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class HotelDetailsVC: UIViewController,MenuControllerDelegate {
   
    @IBOutlet var lblHotelName: UILabel!
    @IBOutlet var lblEventName: UILabel!
    @IBOutlet var lblOffer: UILabel!
    @IBOutlet var lblValidity: UILabel!
    @IBOutlet var lblHotelDescription: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var btnApplyNow: UIButton!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var img_hotelLogo: UIImageView!
    @IBOutlet var viewDetails: UIView!
     @IBOutlet var home: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        home.image = #imageLiteral(resourceName: "home")
        viewDetails.layer.cornerRadius = 10
        viewDetails.clipsToBounds = true
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        Design().SideRoundButton(btn: btnApplyNow)
    }
   
    
    @IBAction func btnApplyNowClick(_ sender: Any) {
       
    }
    @IBAction func btnBackClick(_ sender: Any) {
         self.dismiss(animated: false, completion: nil)
    }
    @IBAction func tabbar(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    
    
}
