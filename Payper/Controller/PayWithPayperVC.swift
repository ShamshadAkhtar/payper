//
//  PayWithPayperVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 13/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class PayWithPayperVC: UIViewController {

    
    
    
    @IBOutlet var lblwallet: UILabel!
    @IBOutlet var lblTotalPayable: UILabel!
    @IBOutlet var lblPayperfee: UILabel!
    @IBOutlet var lblCredit: UILabel!
    @IBOutlet var lblPromotion: UILabel!
    @IBOutlet var lblTip: UILabel!
    @IBOutlet var lblTax: UILabel!
    @IBOutlet var lblSubtotal: UILabel!
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    @IBOutlet var btnApply: UIButton!
    @IBOutlet var btnChange: UIButton!
    @IBOutlet var btnPayper: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Design().SideRoundButton(btn: btnApply)
        Design().SideRoundButton(btn: btnPayper)
        Design().sideCornerRoundView(view: firstView)
        Design().sideCornerRoundView(view: secondView)
        Design().sideCornerRoundButton(btn: btnChange)
    }
    
    @IBAction func btnChangeClick(_ sender: Any) {
    }
    @IBAction func btnApplyClick(_ sender: Any) {
    }
    @IBAction func btnBackClick(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnMenuClick(_ sender: Any) {
    }
    
    @IBAction func btnPayperClick(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
        self.present(vc, animated: false)
    }
    
}
