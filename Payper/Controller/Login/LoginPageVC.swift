//
//  LoginPageVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 14/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class LoginPageVC: UIViewController,UITextFieldDelegate {

    
    @IBOutlet var scrlMain: UIScrollView!
    @IBOutlet var txtemail:UITextField!
    @IBOutlet var txtpassword:UITextField!
    @IBOutlet var emailView:UIView!
    @IBOutlet var passwordView:UIView!
    @IBOutlet var btnlogin:UIButton!
    @IBOutlet var imgview_email: UIImageView!
    @IBOutlet var imgview_password:UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imgview_email.image = #imageLiteral(resourceName: "message")
        imgview_password.image = #imageLiteral(resourceName: "password")
        txtemail.delegate = self
        txtpassword.delegate = self
        txtemail.attributedPlaceholder = NSAttributedString(string: "Email",
                                                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtpassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtpassword.isSecureTextEntry = true
        emailView.underlined()
        passwordView.underlined()
        Design().SideRoundButton(btn: btnlogin)
        Design().textFieldFeture(txt: txtemail)
        Design().textFieldFeture(txt: txtpassword)
    }
    
    
    @IBAction func btnLoginClick(){
        
        print("home")
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
        self.present(vc, animated: false)
        
    }
    @IBAction func btnForgotPassword(){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.present(vc, animated: false)
        
    }
    
    @IBAction func btnRegisterHere(){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "RegisterPageVC") as! RegisterPageVC
        self.present(vc, animated: false)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
