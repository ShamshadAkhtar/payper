//
//  EnterOTPHereVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 11/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class EnterOTPHereVC: UIViewController,MenuControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet var upperTab: UIView!
    @IBOutlet var viewOTP: UIView!
    @IBOutlet var lowerTab: UIView!
    @IBOutlet var txtFirstPin: UITextField!
    @IBOutlet var txtSecondPin: UITextField!
    @IBOutlet var txtThirdPin: UITextField!
    @IBOutlet var txtForthPin: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var btnSideMenu: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        Design().underlined2(txt: txtFirstPin)
        Design().underlined2(txt: txtSecondPin)
        Design().underlined2(txt: txtThirdPin)
        Design().underlined2(txt: txtForthPin)
        Design().textFieldFeture(txt: txtFirstPin)
        Design().textFieldFeture(txt: txtSecondPin)
        Design().textFieldFeture(txt: txtThirdPin)
        Design().textFieldFeture(txt: txtForthPin)
        Design().SideRoundButton(btn: btnSubmit)
        lowerTab.isHidden = true
        viewOTP.layer.cornerRadius = 25
        viewOTP.clipsToBounds = true
        txtFirstPin.delegate = self
        txtSecondPin.delegate = self
        txtThirdPin.delegate = self
        txtForthPin.delegate = self
        txtFirstPin.addTarget(self, action: #selector(textFieldDidChange(txt :)), for: .editingChanged)
        txtSecondPin.addTarget(self, action: #selector(textFieldDidChange(txt :)), for: .editingChanged)
        txtThirdPin.addTarget(self, action: #selector(textFieldDidChange(txt :)), for: .editingChanged)
        txtForthPin.addTarget(self, action: #selector(textFieldDidChange(txt :)), for: .editingChanged)
     
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func tabbar(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    func enterFourDigits(){
        
        if (txtFirstPin.text == "" || txtSecondPin.text == "" || txtThirdPin.text == "" || txtForthPin.text == ""){
            print("null")
        }
        else {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        }
    }
    @IBAction func submitClick(_ sender: Any) {
       enterFourDigits()
    }
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @objc func textFieldDidChange(txt : UITextField){
        
        let text = txt.text
        if text?.utf16.count == 1 {
            
            switch txt{
            case txtFirstPin:
                txtSecondPin.becomeFirstResponder()
            case txtSecondPin:
                txtThirdPin.becomeFirstResponder()
            case txtThirdPin:
                txtForthPin.becomeFirstResponder()
            case txtForthPin:
                txtForthPin.resignFirstResponder()
            default:
                break
            }
        }
        else {
            switch txt{
            case txtFirstPin:
                txtFirstPin.resignFirstResponder()
            case txtSecondPin:
                txtFirstPin.becomeFirstResponder()
            case txtThirdPin:
                txtSecondPin.becomeFirstResponder()
            case txtForthPin:
                txtThirdPin.becomeFirstResponder()
            default:
                break
                
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
