//
//  ForgotPasswordVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 17/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController,UITextFieldDelegate,MenuControllerDelegate {
    @IBOutlet var forgotPassView: UIView!
    @IBOutlet var emailView: UIView!
    @IBOutlet var imag_email: UIImageView!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var btnSideMenu: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        imag_email.image = #imageLiteral(resourceName: "message")
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        txtEmail.delegate = self
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        emailView.underlined()
        Design().SideRoundButton(btn: btnSubmit)
        Design().textFieldFeture(txt: txtEmail)
        forgotPassView.layer.cornerRadius = 25
        forgotPassView.clipsToBounds = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
       return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnSubmitClick(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "EnterOTPHereVC") as! EnterOTPHereVC
        self.present(vc, animated: false)
    }
   
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
   
}
