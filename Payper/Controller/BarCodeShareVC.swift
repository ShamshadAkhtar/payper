//
//  BarCodeShareVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 10/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit


class BarCodeShareVC: UIViewController,MenuControllerDelegate {
   
    

    @IBOutlet var viewUpperTabbar: UIView!
    @IBOutlet var btnPay: UIButton!
    @IBOutlet var lblTotalPrice: UILabel!
    @IBOutlet var infoView: UIView!
    @IBOutlet var menuView: UIView!
    @IBOutlet var connectView: UIView!
    @IBOutlet var shareView: UIView!
    @IBOutlet var btnSideMenu: UIButton!
     @IBOutlet var scan: UIImageView!
      let amount = "AED 500"
    override func viewDidLoad() {
        super.viewDidLoad()
          scan.image = #imageLiteral(resourceName: "scan(color)")
         lblTotalPrice.text = "Total Amount : " + amount
         self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        shareView.backgroundColor = UIColor.init(red: 18/255, green: 149/255, blue: 174/255, alpha: 1)
        Design().SideRoundView(view: viewUpperTabbar)
        Design().SideRoundButton(btn: btnPay)
        
    }
    
    @IBAction func btnPayClick(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        self.present(vc, animated: false)
        
    }
    
    
    @IBAction func btnMenuClick(_ sender: Any) {
    }
    
    
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")

        default:
            break
        }
    }
    
    @IBAction func UpperTabBar(_ sender:UIButton)
    {
        switch sender.tag
        {
        case 0:
            print("info")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
        case 1:
            print("menu")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PayMyBillVC") as! PayMyBillVC
            self.present(vc, animated: false)
        case 2:
            print("connect")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "UserSharingTabVC") as! UserSharingTabVC
            self.present(vc, animated: false)
        case 3:
            print("share")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "BarCodeShareVC") as! BarCodeShareVC
            self.present(vc, animated: false)
        default:
            break
        }
    }
    
    func ShowHideMenuController(_ isShown: Bool) {
         btnSideMenu.isUserInteractionEnabled = !isShown
    }
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
}
