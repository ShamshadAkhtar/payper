//
//  ContactUsVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 19/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController,UITextFieldDelegate,UITextViewDelegate,MenuControllerDelegate {
    
        @IBOutlet var txtName: UITextField!
        @IBOutlet var txtEmail: UITextField!
        @IBOutlet var txtPhone: UITextField!
        @IBOutlet var txtSubject: UITextField!
        @IBOutlet var txtVFeedback: UITextView!
        @IBOutlet var btnSubmit: UIButton!
        @IBOutlet var btnSideMenu: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        txtName.delegate = self
        txtEmail.delegate = self
        txtPhone.delegate = self
        txtSubject.delegate = self
        txtVFeedback.delegate = self
        txtName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)])
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)])
        txtPhone.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)])
        txtSubject.attributedPlaceholder = NSAttributedString(string: "Subject",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)])
        Design().addPaddingAndBorder(to: txtName)
        Design().addPaddingAndBorder(to: txtEmail)
        Design().addPaddingAndBorder(to: txtPhone)
        Design().addPaddingAndBorder(to: txtSubject)
        Design().SideRoundButton(btn: btnSubmit)
        Design().textFieldFeture(txt: txtName)
        Design().textFieldFeture(txt: txtEmail)
        Design().textFieldFeture(txt: txtPhone)
        Design().textFieldFeture(txt: txtSubject)
        txtVFeedback.layer.cornerRadius = 5
        txtVFeedback.clipsToBounds = true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        txtVFeedback.text  = " "
    }
    
    @IBAction func btnSubmitClick(_ sender: Any) {
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
       self.dismiss(animated: false, completion: nil)
        
    }
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    
    // Dismiss the keyboard when the user taps the "Return" key or its equivalent
    // while editing a text field.
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
