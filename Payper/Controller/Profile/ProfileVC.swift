//
//  ProfileVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 17/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit
class ProfileVC: UIViewController ,MenuControllerDelegate,UITextFieldDelegate{
    // MARK:- @IBOutlets
    @IBOutlet var firstView:UIView!
    @IBOutlet var firstNameView:UIView!
    @IBOutlet var secondNameView:UIView!
    @IBOutlet var emailView:UIView!
    @IBOutlet var passwordView:UIView!
     @IBOutlet var editView:UIView!
    @IBOutlet var dateView:UIView!
    @IBOutlet var genderView:UIView!
    @IBOutlet var tipView:UIView!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var imgProfile:UIImageView!
    @IBOutlet var img: UIImageView!
    @IBOutlet var txtFirstName:UITextField!
    @IBOutlet var txtlastName:UITextField!
    @IBOutlet var txtemail:UITextField!
    @IBOutlet var txtPassword:UITextField!
    @IBOutlet var txtdate:UITextField!
    @IBOutlet var txtgender:UITextField!
    @IBOutlet var txtTip:UITextField!
    @IBOutlet var imgFirstName:UIImageView!
    @IBOutlet var imglastName:UIImageView!
    @IBOutlet var imgemail:UIImageView!
    @IBOutlet var imgPassword:UIImageView!
    @IBOutlet var imgdate:UIImageView!
    @IBOutlet var imggender:UIImageView!
    @IBOutlet var imgTip:UIImageView!
    @IBOutlet var btnSideMenu: UIButton!
    
   
     // MARK:-  view life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        imgProfile.image = #imageLiteral(resourceName: "pro2img")
        imgFirstName.image = #imageLiteral(resourceName: "my_account")
        imglastName.image = #imageLiteral(resourceName: "my_account")
        imgemail.image = #imageLiteral(resourceName: "message")
        imgPassword.image = #imageLiteral(resourceName: "password")
        imgdate.image = #imageLiteral(resourceName: "calender")
        imggender.image = #imageLiteral(resourceName: "my_account")
        imgTip.image = #imageLiteral(resourceName: "tip")
        img.image = #imageLiteral(resourceName: "edit_profile")
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        btnSave.isHidden =  true
      //  firstView.underlined()
        Design().changeColor(view: firstNameView)
        Design().changeColor(view: secondNameView)
        Design().changeColor(view: emailView)
        Design().changeColor(view: passwordView)
        Design().changeColor(view: dateView)
        Design().changeColor(view: genderView)
        Design().changeColor(view: tipView)
        Design().sideCornerRoundView(view: firstNameView)
        Design().sideCornerRoundView(view: secondNameView)
        Design().sideCornerRoundView(view: emailView)
        Design().sideCornerRoundView(view: passwordView)
        Design().sideCornerRoundView(view: dateView)
        Design().sideCornerRoundView(view: genderView)
        Design().sideCornerRoundView(view: tipView)
        Design().SideRoundButton(btn: btnSave)
        Design().makeRoundImage(image: imgProfile)
        Design().textFieldFeture(txt: txtFirstName)
        Design().textFieldFeture(txt: txtlastName)
        Design().textFieldFeture(txt: txtemail)
        Design().textFieldFeture(txt: txtPassword)
        Design().textFieldFeture(txt: txtdate)
        Design().textFieldFeture(txt: txtgender)
        Design().textFieldFeture(txt: txtTip)
        txtFirstName.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtlastName.attributedPlaceholder = NSAttributedString(string: "Last Name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtemail.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtdate.attributedPlaceholder = NSAttributedString(string: "Date",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtgender.attributedPlaceholder = NSAttributedString(string: "Gender",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        txtTip.attributedPlaceholder = NSAttributedString(string: "Tip",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        editView.layer.cornerRadius = 5
        editView.clipsToBounds = true
        txtFirstName.isUserInteractionEnabled = false
        txtlastName.isUserInteractionEnabled = false
        txtdate.isUserInteractionEnabled = false
        txtgender.isUserInteractionEnabled = false
        txtTip.isUserInteractionEnabled = false
        txtPassword.isUserInteractionEnabled = false
        txtemail.isUserInteractionEnabled = false
    
        txtFirstName.delegate = self
        txtlastName.delegate = self
        txtdate.delegate = self
        txtgender.delegate = self
        txtTip.delegate = self
        txtPassword.delegate = self
        txtemail.delegate = self
        
        
    }
     // MARK:- buttons actions
    @IBAction func btnSaveClick(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
        self.present(vc, animated: false)
        
    }
    
    
    @IBAction func btnEditClick(_ sender: UIButton) {
        
        btnSave.isHidden =  false
        txtFirstName.isUserInteractionEnabled = true
        txtlastName.isUserInteractionEnabled = true
        txtdate.isUserInteractionEnabled = true
        txtgender.isUserInteractionEnabled = true
        txtTip.isUserInteractionEnabled = true
        txtPassword.isUserInteractionEnabled = true
        txtemail.isUserInteractionEnabled = true
        
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    
    }
     // MARK:- Tabbar
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
           
        default:
            break
        }
    }
     // MARK:- side menu protocols delegates
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
     // MARK:- textfields delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
