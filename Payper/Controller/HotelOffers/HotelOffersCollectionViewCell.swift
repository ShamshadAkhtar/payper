//
//  HotelOffersCollectionViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 16/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class HotelOffersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var img_food: UIImageView!
    @IBOutlet var lbl_food: UILabel!
}
