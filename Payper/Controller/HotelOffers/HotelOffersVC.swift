//
//  HotelOffersVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 16/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class HotelOffersVC: UIViewController,MenuControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource
{
  
    
    
    @IBOutlet var imgHotel: UIImageView!
    @IBOutlet var lblHotelName: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var lblPhoneNumber: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblWebsite: UILabel!
    @IBOutlet var btnSideMenu: UIButton!
    var iArray = ["food1","food2"]
    var nArray = ["buy 1 get 2 free"," get 50% off"]
    override func viewDidLoad() {
        super.viewDidLoad()
         self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
       collectionView.delegate = self
       collectionView.dataSource = self
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! HotelOffersCollectionViewCell
        cell.img_food.image =  UIImage(named: iArray[indexPath.row])
        cell.lbl_food.text = nArray[indexPath.row]
        return cell
    }

}
