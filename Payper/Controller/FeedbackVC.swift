//
//  FeedbackVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 17/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController,MenuControllerDelegate {

    
    @IBOutlet var btnSubmitClick: UIButton!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var rightsign: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Design().SideRoundButton(btn: btnSubmitClick)
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        rightsign.image = #imageLiteral(resourceName: "right sign")
        
    }
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSubmitClick(_ sender: Any) {
    }
    @IBAction func tabbar(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
}
