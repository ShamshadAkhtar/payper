//
//  CreditTableViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 29/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class CreditTableViewCell: UITableViewCell {

    @IBOutlet var lblRestName: UILabel!
    @IBOutlet var lblCredit: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var img_view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
