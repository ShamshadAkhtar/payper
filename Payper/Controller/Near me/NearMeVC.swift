///
//  NearMeVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 11/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class NearMeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MenuControllerDelegate,UITextFieldDelegate{
    
    
    @IBOutlet var tableview:UITableView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var btnSideMenu: UIButton!
    @IBOutlet var searchView : UIView!
    var imgArray : [UIImage] = []
    var eArray = [String]()
    var dArray = [String]()
      override func viewDidLoad() {
        super.viewDidLoad()
         eArray = ["simple dummy text","simple dummy text","simple dummy text","simple dummy text","simple dummy text","simple dummy text","simple dummy text","simple dummy text","simple dummy text","simple dummy text"]
        dArray = ["10 Near you","10 Near you","10 Near you","10 Near you","10 Near you","10 Near you","10 Near you","10 Near you","10 Near you","10 Near you"]
        tableview.delegate = self
        tableview.dataSource = self
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        Design().SideRoundView(view: searchView)
        txtSearch.delegate = self
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search by Establishment",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NearMeTableViewCell
        cell.selectionStyle = .none
        cell.outerview.layer.cornerRadius =  10
        cell.lblEstablistmentName.text = eArray[indexPath.row]
        cell.lblDistance.text = dArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HotelOffersVC") as! HotelOffersVC
            self.present(vc, animated: false)
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
        
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSearchClick(_ sender: Any) {
    }
    
    
    @IBAction func tabbar(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 0:
            print("home")
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
