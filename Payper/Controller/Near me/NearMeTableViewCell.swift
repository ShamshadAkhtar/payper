//
//  NearMeTableViewCell.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 11/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class NearMeTableViewCell: UITableViewCell {
    @IBOutlet var outerview: UIView!
    @IBOutlet var lblEstablistmentName: UILabel!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
}
