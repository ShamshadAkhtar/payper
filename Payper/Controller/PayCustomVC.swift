//
//  PayCustomVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 12/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import UIKit


class PayCustomVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnback: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnPayBill: UIButton!
    @IBOutlet var btnEnterAmount: UIButton!
    @IBOutlet var upperTabBar: UIView!
    @IBOutlet var paybyitem: UIView!
    @IBOutlet var splitequaly: UIView!
    @IBOutlet var paycustom: UIView!
    @IBOutlet var sliderView: UIView!
    @IBOutlet var slider: UISlider!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var lblPercentage: UILabel!
    @IBOutlet var lblvalue: UILabel!
    @IBOutlet var txtEnterAmount: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paycustom.backgroundColor = UIColor.init(red: 18/255, green: 149/255, blue: 174/255, alpha: 1)
        lblvalue.layer.cornerRadius = lblvalue.layer.frame.height / 2
        Design().SideRoundView(view: upperTabBar)
        Design().SideRoundButton(btn: btnNext)
        Design().SideRoundButton(btn: btnPayBill)
        Design().SideRoundButton(btn: btnEnterAmount)
        Design().addPaddingAndBorder2(to: txtEnterAmount)
        Design().textFieldFeture(txt: txtEnterAmount)
        txtEnterAmount.layer.cornerRadius = txtEnterAmount.frame.height / 2
        txtEnterAmount.clipsToBounds = true
        txtEnterAmount.attributedPlaceholder = NSAttributedString(string: "Enter Amount",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)])
        txtEnterAmount.delegate = self
       
        
    }
    
    
    @IBAction func btnNextClick(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "PayWithPayperVC") as! PayWithPayperVC
        self.present(vc, animated: false)
    }
    @IBAction func btnPayBillClick(_ sender: Any) {
    }
    @IBAction func btnEnterAmountClick(_ sender: Any) {
    }
    
    @IBAction func sliderPressed(_ sender: UISlider) {
        DispatchQueue.main.async {
            let trackRect: CGRect  = self.slider.trackRect(forBounds: self.slider.bounds)
            let thumbRect: CGRect  = self.slider.thumbRect(forBounds: self.slider.bounds , trackRect: trackRect, value: self.slider.value)
            let x = thumbRect.origin.x + self.slider.frame.origin.x + 15
            let y = self.slider.frame.origin.y - 10
            let z =  self.slider.frame.origin.y + 40
            self.lblPercentage.center = CGPoint(x: x, y: z)
            self.lblvalue.center = CGPoint(x: x, y: y)
            let currentValue = Int(sender.value)
            self.lblPercentage.text = "\(currentValue)%"
            self.lblvalue.text = "\(currentValue*2)"
        }
        
        
    }
    
    
    @IBAction func menuClick(_ sender: Any) {
    }
    
    @IBAction func backClick(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func UpperTabBar(_ sender:UIButton)
    {
        switch sender.tag
        {
        case 0:
            print("PAY BY ITEM")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            self.present(vc, animated: false)
        case 1:
            print("SPLIT EQUALITY")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SplitEqualyPaymentVC") as! SplitEqualyPaymentVC
            self.present(vc, animated: false)
        case 2:
            print("PAY CUSTOM")
        default:
            break
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
