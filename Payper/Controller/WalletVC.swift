//
//  WalletVC.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 28/01/19.
//  Copyright © 2019 CodeBuzzers Technologies. All rights reserved.
//

import UIKit

class WalletVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MenuControllerDelegate,UITextFieldDelegate {
   
    

    @IBOutlet var lblDefaultPayment: UILabel!
    @IBOutlet var btnAddNew: UIButton!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var searchView: UIView!
    @IBOutlet var payment_Tableview: UITableView!
    @IBOutlet var balance_Tableview: UITableView!
    @IBOutlet var btnSideMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnSideMenu.addTarget(self, action: #selector(self.ShowSideMenu), for: UIControl.Event.touchUpInside)
        txtSearch.delegate = self
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search by Establishment",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        payment_Tableview.separatorStyle = .none
        Design().SideRoundButton(btn: btnAddNew)
        Design().SideRoundView(view: searchView)
        payment_Tableview.delegate = self
        payment_Tableview.dataSource = self
        balance_Tableview.delegate = self
        balance_Tableview.dataSource = self
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnAddNewAction(_ sender: Any) {
    }
    @IBAction func btnSearchClick(_ sender: Any) {
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case payment_Tableview:
            return 2
        case balance_Tableview:
            return 2
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case payment_Tableview:
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WalletTableViewCell
           // cell.img.image = UIImage(named: "<#T##String#>")
           // cell.lblCardName.text =
            return cell
        case balance_Tableview:
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CreditTableViewCell
            cell.img.layer.cornerRadius = 10
            cell.img.clipsToBounds = true
            // cell.img.image = UIImage(named: "<#T##String#>")
            // cell.lblCardName.text =
            return cell
        default: break
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case payment_Tableview:
            return 53
        case balance_Tableview:
            return 100
        default:
            return 0
        }
    }
    @IBAction func tabbar(_ sender: UIButton){
        
        switch sender.tag {
        case 0:
            print("home")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomePageVC") as! HomePageVC
            self.present(vc, animated: false)
        case 1:
            print("scan")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyTabVC") as! MyTabVC
            self.present(vc, animated: false)
            
        case 2:
            print("menu")
        default:
            break
        }
    }
    func ShowHideMenuController(_ isShown: Bool) {
        btnSideMenu.isUserInteractionEnabled = !isShown
    }
    
    @objc func ShowSideMenu() {
        let ObjSideMenu = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuPageVC") as! SideMenuPageVC
        ObjSideMenu.delegate = self
        ObjSideMenu.ShowMenuInController(self)
    }
    
    // MARK:- text fiels delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
