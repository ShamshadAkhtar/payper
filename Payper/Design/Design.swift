//
//  Design.swift
//  Payper
//
//  Created by CodeBuzzers Technologies on 07/12/18.
//  Copyright © 2018 CodeBuzzers Technologies. All rights reserved.
//

import Foundation
import UIKit
class Design {
    
func SideRoundView(view : UIView){
    
    view.layer.cornerRadius = view.frame.height / 2
    view.clipsToBounds = true
    
    }
    func SideRoundButton(btn : UIButton){
        
        btn.layer.cornerRadius = btn.frame.height / 2
        btn.clipsToBounds = true
        
    }
    
    func sideCornerRoundView(view:UIView){
        view.layer.cornerRadius = 10
        view.clipsToBounds =  true
    }
    func sideCornerRoundButton(btn:UIButton){
        btn.layer.cornerRadius = 5
        btn.clipsToBounds =  true
    }
    
    
    func changeColor(view:UIView){
        view.backgroundColor = UIColor(red: 70/255, green: 73/255, blue: 81/255, alpha: 1)
        
    }
    func makeRoundImage(image:UIImageView){
        let myColor : UIColor = UIColor(red: 30/255, green: 172/255, blue: 219/255, alpha: 1)
        image.layer.cornerRadius = (image.frame.size.width ) / 2
        image.layer.borderWidth = 0.3
        image.layer.borderColor = myColor.cgColor
        image.clipsToBounds = true
    }
    func addPaddingAndBorder(to textfield: UITextField) {
        textfield.layer.cornerRadius =  5
        textfield.layer.borderColor = UIColor.black.cgColor
        textfield.layer.borderWidth = 0
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 2.0))
        textfield.leftView = leftView
        textfield.leftViewMode = .always
    }
    func addPaddingAndBorder2(to textfield: UITextField) {
        textfield.layer.cornerRadius =  5
        textfield.layer.borderColor = UIColor.black.cgColor
        textfield.layer.borderWidth = 0
        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 25, height: 2.0))
        textfield.leftView = leftView
        textfield.leftViewMode = .always
    }
    func underlined(txt : UITextField){
    
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: txt.frame.size.height - width, width:  txt.frame.size.width, height: txt.frame.size.height)
        border.borderWidth = width
        txt.layer.addSublayer(border)
        txt.layer.masksToBounds = true
    }
    func underlined2(txt : UITextField){
        let myColor : UIColor = UIColor(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = myColor.cgColor//UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: txt.frame.size.height - width, width:  txt.frame.size.width, height: txt.frame.size.height)
        border.borderWidth = width
        txt.layer.addSublayer(border)
        txt.layer.masksToBounds = true
    }
    func textFieldFeture(txt : UITextField){
        txt.font = UIFont(name:"OpenSans-ExtraBold", size: 24)
    }
}

extension UIView {
    
        func underlined2(){
            let border = CALayer()
            let width = CGFloat(1.0)
            border.borderColor = UIColor(red: 80.0/255.0, green: 83.0/255.0, blue: 91.0/255.0, alpha: 1.0).cgColor
            border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
            border.borderWidth = width
            self.layer.addSublayer(border)
            self.layer.masksToBounds = true
        }
    func underlined(){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0).cgColor// UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}



